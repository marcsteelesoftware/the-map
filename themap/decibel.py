'''
Provides utility methods for dB conversions.
@author: Marc Steele
'''

import math

class Decibel:
    
    '''
    Creates a new instance of the class.
    @param zeroReference: The 0dB reference level in dbFS. 
    '''
    
    def __init__(self, zeroReference=-13):
        zeroReference = float(zeroReference)
        self.__zeroReference = math.pow(10, (zeroReference/10.0)) * 1.0
        
    '''
    @param value: The value to convert.
    '''
        
    def convertToDb(self, value):
        if (value <= 0):
            return 0.0
        return 10.0 * math.log10(value / self.__zeroReference)
    
    def convertToValue(self, db):
        return math.pow(10, (db / 10.0)) * self.__zeroReference