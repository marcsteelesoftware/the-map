'''
The starting point for The MAP.
@author: Marc Steele
'''

import pyaudio
import wave
import numpy as np
import utility
import sys
import time
import struct
from decibel import Decibel
from themap.plugins.Output import Output
from themap.plugins.Limiter import Limiter

# Check we've got a file to play with

if (len(sys.argv) < 2):
    print("Plays a wave file.\n\nUsage: %s filename.wav" % sys.argv[0])
    sys.exit(-1)

# Setup the reference level and DB conversion utility

db = Decibel(-13)

# Setup the callback (processing) chain

def processingCallback(in_data, frame_count, time_info, status):
    
    # Get some frames!
    
    original_data = wave_file.readframes(frame_count)
    
    # We need the samples in float format
    
    data = utility.short2float(original_data, 2)
    
    # Run the audio through the limiter
    
    limiter.input(data)
    limiter.mixInputs()
    limiter.process()
    limiter.output()
    
    # Get it through the output module
    
    output.mixInputs()
    output.process()
    data = output.getOutput()
    
    # Play the file
    
    return (data, pyaudio.paContinue)

# Setup the processing chain

output = Output(2)
limiter = Limiter(1, 10, 4, 44100, db, 2, 0)
limiter.destinations = [output]
    
# Play an audio file

wave_file = wave.open(sys.argv[1], 'rb')
pa = pyaudio.PyAudio()

stream = pa.open(format=pa.get_format_from_width(wave_file.getsampwidth()),
                 channels=wave_file.getnchannels(),
                 rate=wave_file.getframerate(),
                 output=True,
                 stream_callback=processingCallback)

stream.start_stream()

# Keep running the stream

while stream.is_active():
    time.sleep(0.1)

# cleanup

stream.stop_stream()
stream.close()
wave_file.close()
pa.terminate()

