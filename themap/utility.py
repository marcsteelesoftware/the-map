"""
Helper functions for working with audio files in NumPy.
Based on work under CC0.1.0 universal public domain licence from https://github.com/mgeier/python-audio
"""

import struct
import math
import array
import numpy as np
__MAX_SHORT = 32768.0

'''
Converts from short values to floats.
@param data: The data to be converted.
@param channels: The number of channels in the audio stream.
'''

def short2float(data, channels):
    
    # Unpack the chars
    
    format = "%dh" % (len(data) / channels)
    converted_data = list(struct.unpack(format, data))
    
    # Now perform the range conversion
    
    for i in range(len(converted_data)):
        converted_data[i] = converted_data[i] * 1.0 / __MAX_SHORT
    return converted_data

'''
Converts from floating point values to short values
@param data: The data to be converted.
@return: The short values
'''

def float2short(data, channels):

    converted_data = np.zeros(len(data))
       
    for i in np.arange(0, len(data)):
        converted_value = data[i] * 32768.0
        converted_value = max(converted_value, -1.0 * __MAX_SHORT)
        converted_value = min(converted_value, __MAX_SHORT)
        converted_data[i] = converted_value
    
    format = "%dh" % len(converted_data / channels)
    converted = list(struct.pack(format, *list(converted_data)))
    return ''.join(converted)
    