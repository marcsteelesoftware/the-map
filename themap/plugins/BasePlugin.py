'''
The base class all other plugins are based on.
@author: Marc Steele
'''
from themap.plugins.Exceptions import NoAudioInputError, MismatchAudioInputError
import numpy as np

class BasePlugin(object):
    
    def __init__(self):
        self.inputBuffers = []
        self.destinations = []
        self.outputBuffer = []
        self.inputBuffer = []
    
    '''
    Takes the audio from another source as a buffer.
    We can take as many of these as we need. There is a built-in mixing function.
    @param buffer: The new buffer we have coming in. 
    '''
    
    def input(self, buffer):
        self.inputBuffers.append(buffer)
        
    '''
    Performs the mixing of the inputs into an input buffer.
    '''    
        
    def mixInputs(self):
        
        # Ensure we've got some inputs first
        
        if (len(self.inputBuffers) == 0):
            raise NoAudioInputError
        
        # Check the input buffers are all the same length
        
        buffer_length = len(self.inputBuffers[0])
        for buffer in self.inputBuffers:
            if (len(buffer) != buffer_length):
                raise MismatchAudioInputError
            
        # Now perform the mixing
        # We can just add with floating point values
        
        self.inputBuffer = np.zeros(buffer_length)
        for i in np.arange(buffer_length):
            
            current_value = 0.0
            for buffer in self.inputBuffers:
                current_value = current_value + buffer[i]
                
            self.inputBuffer[i] = current_value
            
        self.inputBuffers = []
        
    '''
    Outputs the processing from this stage into the any further stages
    '''
        
    def output(self):
        for destination in self.destinations:
            destination.input(self.outputBuffer)
            
    '''
    Configures the destinations for this stage.
    @param destinations: The destination stages. 
    '''
        
    def setDestinations(self, destinations):
        self.destinations = destinations
        
    '''
    Perform the actual processing step.
    Must be overridden.
    '''
        
    def process(self):
        raise NotImplementedError