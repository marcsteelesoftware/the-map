'''
Implementation of an output to soundcard module.
@author: Marc Steele
'''
from themap.plugins.BasePlugin import BasePlugin
import themap.utility

class Output(BasePlugin):
    
    '''
    Creates a new instance of the output stage.
    @param channels: The number of channels we're working with. 
    '''
    
    def __init__(self, channels):
        BasePlugin.__init__(self)
        self.__channels = channels
        
    '''
    NOP - There is no next stage to process for.
    '''
    
    def output(self):
        return
    
    def process(self):
        self.outputBuffer = themap.utility.float2short(self.inputBuffer, self.__channels)
        
    '''
    Obtains the processed and ready to use output.
    @return: A buffer ready to go directly into PyAudio
    '''    
        
    def getOutput(self):
        return self.outputBuffer
        
        