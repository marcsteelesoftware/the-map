'''
Implementation of a basic limiter.
@author: Marc Steele
'''

import numpy as np
from themap.plugins.BasePlugin import BasePlugin
from numpy import arange
import math

class Limiter(BasePlugin):   
    
    '''
    Creates a new instance of the limiter
    @param attack: The attack time in ms.
    @param release: The release time in ms.
    @param delay: The look-ahead delay time in ms.
    @param sample_rate: The sample rate of the audio.
    @param db: The dB settings
    @param channels: The number of channels the audio is in.
    @parm threshold: The threshold level in dB.
    '''
    
    def __init__(self, attack, release, delay, sample_rate, db, channels, threshold):
        
        BasePlugin.__init__(self)
        
        # Calculate the samples per ms
        
        samples_per_ms = sample_rate / 1000
        
        # Use it to calculate the important information in samples
        
        self.__attack_coefficient = math.pow(math.e, (-1.0 * attack * samples_per_ms * channels))
        self.__release_coefficient = math.pow(math.e, (-1.0 * release * samples_per_ms * channels))
        self.__delay = delay * samples_per_ms * channels
        self.__delay_index = 0
        self.__envelope = 0
        self.__delay_line = np.zeros(self.__delay)
        self.__db = db
        self.__gain = 1.0
        
        # Calculate the threshold
        
        self.__threshold = self.__db.convertToValue(threshold)
    
    '''
    Performs the limiting operation.
    '''
    
    def process(self):
        
        self.outputBuffer = np.zeros(len(self.inputBuffer))
        
        for i in arange(len(self.inputBuffer)):
            
            # Delay the output (lookahead)
            
            self.__delay_line[self.__delay_index] = self.inputBuffer[i]
            self.__delay_index = (self.__delay_index + 1) % self.__delay
            
            # Calculate the envelope
            
            self.__envelope = self.__envelope * self.__release_coefficient
            self.__envelope = max(abs(self.inputBuffer[i]), self.__envelope)
            
            # Work out our target gain
            
            if (self.__envelope > self.__threshold):
                target_gain = (1.0 + self.__threshold - self.__envelope)
            else:
                target_gain = 1.0
                
            self.__gain = (self.__gain * self.__attack_coefficient + target_gain * (1.0 - self.__attack_coefficient))
            
            # Limit the delayed signal
            
            self.outputBuffer[i] = self.__delay_line[self.__delay_index] * self.__gain
                
            
            
             
        