'''
The possible exceptions that can be raised by the processor.
@author: Marc Steele
'''

class NoAudioInputError(Exception):
    def __str__(self):
        return 'There is no audio in the input buffers. Unable to continue.'
    
class MismatchAudioInputError(Exception):
    def __str__(self):
        return 'The audio input buffers do not match in length. Cannot mix or continue processing.'